import pygame, sys
from pygame.locals import *

#import states.TitleState

from states import GameState
from Ezha import ConfigManager
from Ezha import FontManager
from Ezha import LanguageManager

class Program( object ):
    
    def __init__( self ):
        self.fpsClock = None
        self.windowSurface = None
        self.screenWidth = 320
        self.screenHeight = 320
        self.isDone = False
        self.fps = 60
        self.clearColor = pygame.Color( 151, 255, 140 ) 
    
    def Init( self ):
        pygame.init()
        
        pygame.display.set_caption( "Pickin' Sticks - Ezha" )
        FontManager.Add( "main", "assets/NotoSans-Regular.ttf", 20 )
        
        self.fpsClock = pygame.time.Clock()
        self.windowSurface = pygame.display.set_mode( ( self.screenWidth, self.screenHeight ) )

        self.currentState = GameState()
        self.currentState.Setup( self.screenWidth, self.screenHeight )
        
    def Run( self ):
        while ( self.isDone is False ):
            self.Update()
            self.Draw()
    
    def Update( self ):
        self.currentState.Update()
        
    def Draw( self ):
        self.windowSurface.fill( self.clearColor )
        
        self.currentState.Draw( self.windowSurface )
        
        pygame.display.update()
        self.fpsClock.tick( self.fps )
