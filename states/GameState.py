#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import math

import pygame, sys
from pygame.locals import *

from Ezha import BaseState
from Ezha import FontManager
from Ezha import LanguageManager
from Ezha import ConfigManager
from Ezha import Button
from Ezha import Image
from Ezha import Label
from objects import Tile
from objects import Character

class GameState( BaseState ):
    def __init__( self ):
        self.stateName = "titlestate"

    def GetDistance( self, rectA, rectB ):
        dx = rectB.x - rectA.x
        dy = rectB.y - rectA.y
        return math.sqrt( dx ** 2 + dy ** 2 )
        
    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        
        self.scoreColor = pygame.Color( 255, 255, 255 ) 
        
        self.images = {
            "grass"     : pygame.image.load( "assets/grass.png" ),
            "rabbit"    : pygame.image.load( "assets/rabbit.png" ),
            "stick"     : pygame.image.load( "assets/stick.png" ),
        }

        self.backgroundTiles = []
        for y in range( self.screenHeight / 16 ):
            for x in range( self.screenWidth / 16 ):
                tile = Tile()
                tile.Setup( self.images["grass"], x * 16, y * 16, 16, 16 )

                rando = random.randint( 0,7 )
                if ( rando == 0 ):
                    tile.SetupFrame( 16, 0, 16, 16 )
                self.backgroundTiles.append( tile )

        self.player = Character()
        self.player.Setup( self.images["rabbit"], self.screenWidth/2, self.screenHeight/2, 16, 16 )

        self.stick = Character()
        self.stick.Setup( self.images["stick"], random.randint( 0, self.screenWidth-16 ), random.randint( 0, self.screenHeight-16 ), 16, 16 )

        self.scoreText = FontManager.Get( "main" ).render( "Score: 0", False, self.scoreColor )

        self.collectSound = pygame.mixer.Sound( "assets/collect.wav" )
        
    def Update( self ):     
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            if event.type == MOUSEBUTTONUP:
                mousex, mousey = event.pos

        keys = pygame.key.get_pressed()
        if ( keys[ K_UP ] ):        self.player.Move( "UP" )
        elif ( keys[ K_DOWN ] ):    self.player.Move( "DOWN" )
        elif ( keys[ K_LEFT ] ):    self.player.Move( "LEFT" )
        elif ( keys[ K_RIGHT ] ):   self.player.Move( "RIGHT" )

        if ( self.GetDistance( self.player.posRect, self.stick.posRect ) <= 10 ):
            self.stick.SetPosition( random.randint( 0, self.screenWidth-16 ), random.randint( 0, self.screenHeight-16 ) )
            self.player.IncrementScore( 1 )
            self.scoreText = FontManager.Get( "main" ).render( "Score: " + str( self.player.GetScore() ), False, self.scoreColor )
            self.collectSound.play()
    
    
    def Draw( self, windowSurface ):
        for tile in self.backgroundTiles:
            tile.Draw( windowSurface )

        self.stick.Draw( windowSurface )
        self.player.Draw( windowSurface )

        windowSurface.blit( self.scoreText, ( 10, 10 ) )
    
