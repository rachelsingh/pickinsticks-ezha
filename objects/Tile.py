#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

class Tile:
    def __init__( self ):
        self.posRect = pygame.Rect( 0, 0, 16, 16 )
        self.frameRect = pygame.Rect( 0, 0, 16, 16 )
        self.image = None

    def Setup( self, image, x, y, w, h ):
        self.image = image
        self.posRect = pygame.Rect( x, y, w, h )

    def SetupFrame( self, x, y, w, h ):
        self.frameRect.x = x
        self.frameRect.y = y
        self.frameRect.width = w
        self.frameRect.height = h

    def Draw( self, window ):
        if ( self.image is not None ):
            window.blit( self.image, self.posRect, self.frameRect )
