#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from Tile import Tile

class Character( Tile ):
    def __init__( self ):
        self.posRect = pygame.Rect( 0, 0, 16, 16 )
        self.frameRect = pygame.Rect( 0, 0, 16, 16 )
        self.image = None
        self.frame = 0
        self.maxFrame = 2
        self.speed = 2
        self.score = 0

    def Setup( self, image, x, y, w, h ):
        self.image = image
        self.posRect = pygame.Rect( x, y, w, h )

    def SetPosition( self, x, y ):
        self.posRect.x = x
        self.posRect.y = y

    def SetupFrame( self, x, y, w, h ):
        self.frameRect.x = x
        self.frameRect.y = y
        self.frameRect.width = w
        self.frameRect.height = h

    def IncrementScore( self, amount ):
        self.score = self.score + amount

    def GetScore( self ):
        return self.score

    def Draw( self, window ):
        if ( self.image is not None ):
            window.blit( self.image, self.posRect, self.frameRect )

    def Move( self, direction ):
        if ( direction == "UP" ):
            self.posRect.y = self.posRect.y - self.speed
        elif ( direction == "DOWN" ):
            self.posRect.y = self.posRect.y + self.speed
        elif ( direction == "LEFT" ):
            self.posRect.x = self.posRect.x - self.speed
        elif ( direction == "RIGHT" ):
            self.posRect.x = self.posRect.x + self.speed
